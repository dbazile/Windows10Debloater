# win_debloater

[![made-with-powershell](https://img.shields.io/badge/PowerShell-1f425f?logo=Powershell)](https://microsoft.com/PowerShell)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

> Cleaned up fork of [https://github.com/Sycnex/Windows10Debloater](https://github.com/Sycnex/Windows10Debloater)


## Usage

Download [win_debloater.ps1](win_debloater.ps1) and inspect the source code to
ensure there're no shenanigans (it's a few hundred lines and optimized for
visual source code auditing). If satisfied, open an instance of Powershell
with administrative permissions and execute:

```
Set-ExecutionPolicy -Scope CurrentUser Unrestricted
win_debloater.ps1
```


## Caveats

I only refactored the main script. The other portions of this repo are only here
for documentation purposes and are not part of the actual execution flow
anymore.

Also, this script is provided as-is for you to use at your own risk.


## Credits

Thank you to [the original author(s)](https://github.com/Sycnex/Windows10Debloater).
Other aspects were inspired by [Chris Titus' `winutil`](https://github.com/ChrisTitusTech/winutil/blob/main/winutil.ps1)
and [@raphire's Win11Debloat](https://github.com/Raphire/Win11Debloat/blob/master/Win11Debloat.ps1).

